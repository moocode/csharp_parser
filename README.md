# csharp_parser

experimental C# parser done mostly to learn how to create parsers...
based on awesome http://www.craftinginterpreters.com 

Current main purpose is converting POCOs to PODOs
(plain old C#/Dart object) files

Warning! It's not production ready

## Usage
 
```dart in html
var scanner = new Scanner(sourceCode);
scanner.scanTokens();
print('is error: ${scanner.hadError}');
Parser parser = new Parser(scanner.tokens);
 
List<Stmt> statements = parser.parse();
//now statements contains AST tree
```
  
## Features and bugs
Recognizes namespace class, properties, methods

### Current limitations
- ignores method bodies
- does not support conditional expressions
- does not support namespace aliases in using directive

 
