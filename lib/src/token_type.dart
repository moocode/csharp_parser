// Copyright (c) 2017, Tomasz Kubacki. All rights reserved. Use of this source code
// is governed by a MIT-style license that can be found in the LICENSE file.

enum TokenType {
  // Single-character tokens.
  LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE,
  LEFT_SQUARE_BRACE,RIGHT_SQUARE_BRACE, COMMA, DOT,
  MINUS, PLUS, COLON, SEMICOLON, SLASH, STAR,
  QUESTION_MARK, LESS_THAN,GREATER_THAN,

  // One or two character tokens.
  BANG, BANG_EQUAL,
  EQUAL, EQUAL_EQUAL,
  GREATER, GREATER_EQUAL,
  LESS, LESS_EQUAL,

  // Literals.
  IDENTIFIER, STRING, NUMBER,

  // Keywords.
  AND, NEW, STATIC, PUBLIC,PRIVATE, OVERRIDE,
  CLASS, ELSE, FALSE,  FOR, IF, NULL, OR, REF, OUT,
  PRINT, RETURN, SUPER, THIS, TRUE, VAR, WHILE,
  USING, GET,SET,NAMESPACE, EOF
}
