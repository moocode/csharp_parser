// Copyright (c) 2017, Tomasz Kubacki. All rights reserved. Use of this source code
// is governed by a MIT-style license that can be found in the LICENSE file.
import 'package:csharp_parser/src/token.dart';
import 'package:csharp_parser/src/token_type.dart';

class Parser {
  final List<Token> tokens;
  int current = 0;

  Parser(this.tokens);

  bool match(List<TokenType> types) {
    for (TokenType type in types) {
      if (check(type)) {
        advance();
        return true;
      }
    }
    return false;
  }

  bool check(TokenType tokenType) {
    if (isAtEnd()) return false;
    return peek().type == tokenType;
  }

  Token advance() {
    if (!isAtEnd()) current++;
    return previous();
  }

  bool isAtEnd() {
    return peek().type == TokenType.EOF;
  }

  Token peek() {
    return tokens[current];
  }

  Token previous() {
    return tokens[current - 1];
  }

  Stmt usingDirective() {
    List<Token> tokens = [];
    tokens.add(consume(TokenType.IDENTIFIER, "Expect identifier"));
    while (!check(TokenType.SEMICOLON)) {
      consume(TokenType.DOT, "expected dot and identifier");
      tokens.add(consume(TokenType.IDENTIFIER, "expected identifier"));
    }
    consume(TokenType.SEMICOLON, "expected smicolon");
    return new UsingDirective(tokens);
  }

  Stmt namespace() {
    Token namespaceName = consume(TokenType.IDENTIFIER, "Expect identifier");

    consume(TokenType.LEFT_BRACE, "Expect left brace");

    List<Class> classes = [];

    while (!check(TokenType.RIGHT_BRACE)) {
      while(attribute());
      match([TokenType.PUBLIC, TokenType.PRIVATE]);
      if (check(TokenType.CLASS)) {
        classes.add(classStatement());
      }
    }
    consume(TokenType.RIGHT_BRACE, "Expect right bracket");

    return new Namespace(namespaceName, classes);
  }

  Class classStatement() {
    bool isPublic = false;

    if (previous().type == TokenType.PUBLIC) isPublic = true;

    List<Property> properties = [];
    List<Method> methods = [];

    List<Variable> fields = [];

    consume(TokenType.CLASS, "Expect 'class' keyword");
    Token className =
        consume(TokenType.IDENTIFIER, "Expect class name identifier");
    Type baseType = null;
    if (match([TokenType.COLON])) {
      baseType = type();
    }
    consume(TokenType.LEFT_BRACE, "Expect left bracket");

    while (!match([TokenType.RIGHT_BRACE])) {
      while(attribute());

      match([TokenType.PUBLIC, TokenType.PRIVATE]);
      match([TokenType.STATIC]);
      match([TokenType.OVERRIDE]);

      Type typeFound = type();
      Token name = consume(TokenType.IDENTIFIER, "Expect identifier");

      if (match([TokenType.LEFT_BRACE])) {
        properties.add(property(name, typeFound));
      } else if (check(TokenType.LEFT_PAREN)) {
        methods.add(method(name, typeFound));
      } else if (check(TokenType.SEMICOLON) || check(TokenType.EQUAL)) {
        fields.add(varDeclaration(typeFound, name));
      }
    }

    Class c =
        new Class(className, fields, properties, methods, baseType, isPublic);

    return c;
  }

  bool attribute(){
    if (match([TokenType.LEFT_SQUARE_BRACE])) {
      while (!match([TokenType.RIGHT_SQUARE_BRACE])) {
        advance();
      }
      //TODO
      // support multiple attributes for
      // fields, methods, properties
      return true;
    }
    return false;
  }

  Stmt varDeclaration(Type type, Token name) {
    Expr initializer = null;
    if (match([TokenType.EQUAL])) {
      initializer = expression();
    }

    consume(TokenType.SEMICOLON, "Expect ';' after variable declaration.");
    return new Variable(type, name, initializer);
  }

  Method method(Token name, Type returnType) {
    List<Parameter> parameters = [];
    Block body = null;

    consume(TokenType.LEFT_PAREN, "Expect opening parenthesis");

    while (!check(TokenType.RIGHT_PAREN) && !isAtEnd()) {
      if (check(TokenType.IDENTIFIER)) {
        Type t = type();
        Token paramName =
            consume(TokenType.IDENTIFIER, 'Expect parameter name');
        match([TokenType.COMMA]);
        parameters.add(new Parameter(paramName, t));
      }
    }
    advance();

    if (match([TokenType.LEFT_BRACE])) {
      body = mockBlock();
    }

    var method = new Method(name, returnType, parameters, body);
    return method;
  }

  Property property(Token name, Type type) {
    Stmt getter = null;
    Stmt setter = null;

    while (!check(TokenType.RIGHT_BRACE) && !isAtEnd()) {
      match([TokenType.PUBLIC, TokenType.PRIVATE]);

      if (match([TokenType.GET, TokenType.SET])) {
        Token getterOrSetter = previous();
        Stmt expression = null;
        if (match([TokenType.LEFT_BRACE])) {
            expression = block();
        } else {
          expression = new Block([]);
          consume(TokenType.SEMICOLON, "Expect semicolon or left bracket");
        }
        if (getterOrSetter.type == TokenType.GET) {
          getter = expression;
        } else if (getterOrSetter.type == TokenType.SET) {
          setter = expression;
        }
      }
    }
    consume(TokenType.RIGHT_BRACE, "Expect right bracket");
    Expr initializer = null;
    if (match([TokenType.EQUAL])) {
      initializer = expression();
      consume(TokenType.SEMICOLON, "Expect semicolon");
    }

    var prop = new Property(name, type, getter, setter, initializer);
    return prop;
  }

  Type type() {

    Token typeDef = consume(TokenType.IDENTIFIER, "Expect type name");

    List<Type> genericTypes = [];
    if (match([TokenType.LESS_THAN])) {
      while (!match([TokenType.GREATER_THAN])) {
        var genericType = type();
        genericTypes.add(genericType);
        match([TokenType.COMMA]);
      }
    }
    match([TokenType.QUESTION_MARK]);
    
    return new Type(typeDef, genericTypes);
  }

  Stmt declaration() {
    if (match([TokenType.USING])) {
      return usingDirective();
    } else if (match([TokenType.NAMESPACE])) {
      return namespace();
    }
    return statement();
  }

  Expr equality() {
    Expr expr = comparison();

    while (match([TokenType.BANG_EQUAL, TokenType.EQUAL_EQUAL])) {
      Token operator = previous();
      Expr right = comparison();
      expr = new Binary(expr, operator, right);
    }

    return expr;
  }

  Expr comparison() {
    Expr expr = addition();

    while (match([
      TokenType.GREATER,
      TokenType.GREATER_EQUAL,
      TokenType.LESS_THAN,
      TokenType.LESS_EQUAL
    ])) {
      Token operator = previous();
      Expr right = addition();
      expr = new Binary(expr, operator, right);
    }

    return expr;
  }

  Stmt statement() {
    if (match([TokenType.FOR])) return forStatement();
    if (match([TokenType.IF])) return ifStatement();

    if (match([TokenType.RETURN])) return returnStatement();
    if (match([TokenType.WHILE])) return whileStatement();
    if (match([TokenType.LEFT_BRACE])) return block();
    if (check(TokenType.IDENTIFIER)) {
      Type typeFound = type();

      if (match([TokenType.EQUAL])) {
        return assignment();
      } else {
        Token name = consume(TokenType.IDENTIFIER, "Expect variable name");
        return varDeclaration(typeFound, name);
      }
    }
    return expressionStatement();
  }

  Stmt expressionStatement() {
    Expr expr = expression();
    consume(TokenType.SEMICOLON, "Expect ';' after expression.");
    return new Expression(expr);
  }

  Expr assignment() {

    Expr expr = or();

    if (match([TokenType.EQUAL])) {
      Token equals = previous();
      Expr value = assignment();

      if (expr is VariableValue) {
        Token name = (expr as Variable).variableName;
        return new Assign(name, value);
      } // else if (expr is Get) {
        //Get get = (Expr.Get)expr;
        //return new Expr.Set(get.object, get.name, value);
        //}

      //error(equals, "Invalid assignment target.");
    }

    return expr;
  }

  Expr or() {
    Expr expr = and();

    while (match([TokenType.OR])) {
      Token operator = previous();
      Expr right = and();
      expr = new Logical(expr, operator, right);
    }

    return expr;
  }

  Expr and() {
    Expr expr = equality();

    while (match([TokenType.AND])) {
      Token operator = previous();
      Expr right = equality();
      expr = new Logical(expr, operator, right);
    }

    return expr;
  }

  forStatement() {}

  ifStatement() {
    consume(TokenType.LEFT_PAREN, "Expect '(' after 'if'.");
    Expr condition = expression();
    consume(
        TokenType.RIGHT_PAREN, "Expect ')' after if condition."); // [parens]

    Stmt thenBranch = statement();
    Stmt elseBranch = null;
    if (match([TokenType.ELSE])) {
      elseBranch = statement();
    }

    return new If(condition, thenBranch, elseBranch);
  }

  Stmt returnStatement() {
    Token keyword = previous();
    Expr value = null;
    if (!check(TokenType.SEMICOLON)) {
      value = expression();
    }

    consume(TokenType.SEMICOLON, "Expect ';' after return value.");
    return new Return(value);
  }

  whileStatement() {}

  Block block() {
    List<Stmt> statements = [];

    while (!check(TokenType.RIGHT_BRACE) && !isAtEnd()) {
      statements.add(declaration());
    }

    consume(TokenType.RIGHT_BRACE, "Expect '}' after block.");
    return new Block(statements);
  }

  Block mockBlock() {
    List<Stmt> statements = [];

    while (!check(TokenType.RIGHT_BRACE) && !isAtEnd()) {
      advance();
    }

    consume(TokenType.RIGHT_BRACE, "Expect '}' after block.");
    return new Block(statements);
  }

  Expr addition() {
    Expr expr = multiplication();

    while (match([TokenType.MINUS, TokenType.PLUS])) {
      Token operator = previous();
      Expr right = multiplication();
      expr = new Binary(expr, operator, right);
    }

    return expr;
  }

  Expr multiplication() {
    Expr expr = unary();

    while (match([TokenType.SLASH, TokenType.STAR])) {
      Token operator = previous();
      Expr right = unary();
      expr = new Binary(expr, operator, right);
    }

    return expr;
  }

  Expr unary() {
    if (match([TokenType.BANG, TokenType.MINUS])) {
      Token operator = previous();
      Expr right = unary();
      return new Unary(operator, right);
    }

    return primary();
  }

  Expr call(Type type) {
    List<Expr> arguments = [];
    consume(TokenType.LEFT_PAREN, 'Expect opening parenthesis');
    while (!match([TokenType.RIGHT_PAREN])) {
      arguments.add(expression());
      match([TokenType.COMMA]);
    }

    return new Call(arguments, type);
  }

  Expr expression() {
    return assignment();
  }

  Expr primary() {
    if (match([TokenType.FALSE])) return new Literal(false);
    if (match([TokenType.TRUE])) return new Literal(true);
    if (match([TokenType.NULL])) return new Literal(null);

    if (match([TokenType.NUMBER, TokenType.STRING])) {
      return new Literal(previous().literal);
    }

    if (match([TokenType.NEW])) {
      Type t = type();
      return call(t);
    }

    match([TokenType.REF, TokenType.OUT]);

    if (check(TokenType.IDENTIFIER)) {
      Type t = type();

      if (check(TokenType.LEFT_PAREN)) {
        return call(t);
      } else {
        return new VariableValue(t.type);
      }
    }

    if (match([TokenType.LEFT_PAREN])) {
      Stmt expr = expression();
      consume(TokenType.RIGHT_PAREN, "Expect ')' after expression.");
      return new Grouping(expr);
    }
  }

  Token consume(TokenType type, String message) {
    if (check(type)) return advance();

    throw error(peek(), message);
  }

  ParseError error(Token token, String message) {
    print(message);
    return new ParseError();
  }

  List<Stmt> parse() {
    List<Stmt> statements = [];
    try {
      while (!isAtEnd()) {
        statements.add(declaration());
      }
    } catch (err) {
      return null;
    }
    return statements;
  }
}

abstract class Visitor<R> {
  R visitType(Type type);
  R visitParameter(Parameter parameter);
  R visitBlockStmt(Block block);
  R visitClassStmt(Class classStmt);
  R visitProperty(Property property);
  R visitMethod(Method method);
  R visitVariable(Variable variable);
  R visitVariableValue(VariableValue variableValue);
  R visitReturn(Return returnStm);
  R visitUsing(UsingDirective usingDirective);
  R visitNamespace(Namespace namespace);
  R visitExpr(Expr expr);
  R visitCall(Call call);
  R visitIf(If ifstmt);
}

class Expression extends Stmt {
  Expression(Expr expression) : super("") {
    this.expression = expression;
  }

  Expr expression;
}

// ignore: non_generative_constructor
class ParseError {}

class Stmt {
  Stmt(this.typeName);
  final String typeName;
}

class Type extends Stmt {
  Type(this.type, this.generics) : super("type");

  final Token type;

  final List<Type> generics;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitType(this);
  }
}

class Block extends Stmt {
  Block(this.statements) : super("block");
  List<Stmt> statements = [];

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitBlockStmt(this);
  }
}

class Property extends Stmt {
  Property(
      this.propertyName, this.type, this.getter, this.setter, this.initializer)
      : super("property");
  Token propertyName;
  Type type;
  Stmt getter;
  Stmt setter;
  Expr initializer;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitProperty(this);
  }
}

class Method extends Expr {
  Method(this.methodName, this.returnType, this.parameters, this.body)
      : super("method");
  Type returnType;
  Token methodName;
  List<Parameter> parameters;
  Block body;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitMethod(this);
  }
}

class Parameter extends Stmt {
  Parameter(this.parameterName, this.type) : super("parameter");
  Type type;
  Token parameterName;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitParameter(this);
  }
}

class Variable extends Stmt {
  Variable(this.type, this.variableName, this.initializer) : super("variable");

  Type type;

  Token variableName;

  Expr initializer;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitVariable(this);
  }
}

class VariableValue extends Expr {
  VariableValue(this.variable) : super("variable_value");

  final Token variable;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitVariableValue(this);
  }
}

class UsingDirective extends Stmt {
  UsingDirective(this.namespace) : super("using");
  final List<Token> namespace;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitUsing(this);
  }
}

class Namespace extends Stmt {
  Namespace(this.namespace, this.classes) : super("namespace");

  Token namespace;
  List<Class> classes = [];

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitNamespace(this);
  }
}

class Class extends Stmt {
  bool isPublic;
  Class(this.className, this.fields, this.properties, this.methods,
      this.baseType, this.isPublic)
      : super('class');
  final Type baseType;
  final Token className;
  final List<Variable> fields;
  final List<Property> properties;
  final List<Method> methods;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitClassStmt(this);
  }
}

class Return extends Stmt {
  Return(this.expression) : super("return");

  final Expr expression;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitReturn(this);
  }
}

class If extends Stmt {
  If(this.condition, this.thenBranch, this.elseBranch) : super("if");

  final Expr condition;
  final Expr thenBranch;
  final Expr elseBranch;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitIf(this);
  }
}

class Assign extends Expr {
  Assign(this.name, this.value) : super("assign");

  final Token name;
  final Expr value;
}

abstract class Expr extends Stmt {
  Expr(String typeName) : super(typeName);

  Expr left;
  Token operator;
  Expr right;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitExpr(this);
  }
}

class Call extends Expr {
  Call(this.arguments, this.type) : super("call");
  final List<Expr> arguments;
  final Type type;

  R accept<R>(Visitor<R> visitor) {
    return visitor.visitCall(this);
  }
}

class Grouping extends Expr {
  Grouping(Expr expr) : super("grouping") {
    left = expr;
  }
}

class Unary extends Expr {
  Unary(Token operator, Expr right) : super("unary") {
    this.operator = operator;
    this.right = right;
  }
}

class Literal extends Expr {
  Object value;
  Literal(Object val) : super("literal") {
    value = val;
  }
}

class Logical extends Expr {

  Logical(this.left,this.operator,this.right): super("logical");

  final Expr left;
  final Token operator;
  final Expr right;
}

class Binary extends Expr {
  Binary(Expr left, Token operator, Expr right) : super("binary") {
    this.left = left;
    this.operator = operator;
    this.right = right;
  }
}
