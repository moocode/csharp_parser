// Copyright (c) 2017, Tomasz Kubacki. All rights reserved. Use of this source code
// is governed by a MIT-style license that can be found in the LICENSE file.

import 'package:csharp_parser/src/token_type.dart';

class Token {
  final TokenType type;
  final String lexeme;
  final Object literal;
  final int line;

  Token(this.type, this.lexeme, this.literal, this.line);

  String toString() {
    return "$type $lexeme ${literal != null ? literal : ''}";
  }
}
