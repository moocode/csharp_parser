// Copyright (c) 2017, Tomasz Kubacki. All rights reserved. Use of this source code
// is governed by a MIT-style license that can be found in the LICENSE file.

import 'package:csharp_parser/csharp_parser.dart';

class AstPrinter extends Visitor<String> {
  @override
  String visitBlockStmt(Block block) {
    return block.typeName;
  }

  @override
  String visitClassStmt(Class classStmt) {
    return classStmt.className.lexeme;
  }

  @override
  String visitMethod(Method method) {
    return method.methodName.lexeme;
  }

  @override
  String visitNamespace(Namespace namespace) {
    return  namespace.namespace.lexeme;
  }

  @override
  String visitParameter(Parameter parameter) {
    return parameter.typeName;
  }

  @override
  String visitProperty(Property property) {
    return property.propertyName.lexeme;
  }

  @override
  String visitReturn(Return returnStm) {
    return returnStm.typeName;
  }

  @override
  // ignore: conflicting_dart_import
  String visitType(Type type) {
    return type.type.lexeme;
  }

  @override
  String visitUsing(UsingDirective usingDirective) {
    return usingDirective.typeName;
  }

  @override
  String visitVariable(Variable variable) {
    return variable.variableName.lexeme;
  }

  @override
  String visitVariableValue(VariableValue variableValue) {
    return variableValue.typeName;
  }

  @override
  String visitCall(Call call) {
    return call.typeName + " " + visitType(call.type);
  }

  @override
  String visitExpr(Expr expr) {
    //TODO show something useful
    return "";
  }
  @override
  String visitIf(If ifstmt) {
    // TODO: implement visitIf
    return "";
  }
}
