// Copyright (c) 2017, Tomasz Kubacki. All rights reserved. Use of this source code
// is governed by a MIT-style license that can be found in the LICENSE file.

import 'package:csharp_parser/src/token.dart';
import 'package:csharp_parser/src/token_type.dart';

class Scanner {
  bool hadError = false;

  final List<Token> tokens = new List();
  int start = 0;
  int current = 0;
  int line = 1;
  String source = '';

  Scanner(this.source);

  List<Token> scanTokens() {
    while (!isAtEnd()) {
      start = current;
      scanToken();
    }

    tokens.add(new Token(TokenType.EOF, "", null, line));
    return tokens;
  }

  String advance() {
    current++;
    return source[current - 1];
  }

  String peek() {
    if (isAtEnd()) return '\0';
    return source[current];
  }

  String peekNext() {
    if (current + 1 >= source.length) return '\0';
    return source[current + 1];
  }

  scanToken() {
    String c = advance();
    switch (c) {
      case '(':
        addToken(TokenType.LEFT_PAREN);
        break;
      case ')':
        addToken(TokenType.RIGHT_PAREN);
        break;
      case '{':
        addToken(TokenType.LEFT_BRACE);
        break;
      case '}':
        addToken(TokenType.RIGHT_BRACE);
        break;
      case '[':
        addToken(TokenType.LEFT_SQUARE_BRACE);
        break;
      case ']':
        addToken(TokenType.RIGHT_SQUARE_BRACE);
        break;
      case ',':
        addToken(TokenType.COMMA);
        break;
      case '.':
        addToken(TokenType.DOT);
        break;
      case '-':
        addToken(TokenType.MINUS);
        break;
      case '+':
        addToken(TokenType.PLUS);
        break;
      case '<':
        addToken(TokenType.LESS_THAN);
        break;
      case '>':
        addToken(TokenType.GREATER_THAN);
        break;
      case ':':
        addToken(TokenType.COLON);
        break;
      case ';':
        addToken(TokenType.SEMICOLON);
        break;
      case '*':
        addToken(TokenType.STAR);
        break;
      case '?':
        addToken(TokenType.QUESTION_MARK);
        break;
      case '!':
        addToken(match('=') ? TokenType.BANG_EQUAL : TokenType.BANG);
        break;
      case '&': addToken(match('&') ? TokenType.AND : TokenType.AND); break;
      case '|': addToken(match('|') ? TokenType.AND : TokenType.AND); break;
      case '=': addToken(match('=') ? TokenType.EQUAL_EQUAL : TokenType.EQUAL); break;
      case '<': addToken(match('=') ? TokenType.LESS_EQUAL : TokenType.LESS); break;
      case '>': addToken(match('=') ? TokenType.GREATER_EQUAL : TokenType.GREATER); break;
      case '/':
        if (match('/')) {
          while (peek() != '\n' && !isAtEnd()) advance();
        } else {
          addToken(TokenType.SLASH);
        }
        break;
      case '"':
        string();
        break;
      case ' ':
      case '\r':
      case '\t':
        break;
      case '\n':
        line++;
        break;
      default:
        if (isDigit(c)) {
          number();
        } else if (isAlpha(c)) {
          identifier();
        } else {
          error(line, "Unexpected character. $c");
        }
        break;
    }
  }

  void addToken(TokenType type) {
    addTokenWithLiteral(type, null);
  }

  bool match(String expected) {
    if (isAtEnd()) return false;
    if (source[current] != expected) return false;

    current++;
    return true;
  }

  void addTokenWithLiteral(TokenType type, Object literal) {
    String text = source.substring(start, current);
    tokens.add(new Token(type, text, literal, line));
  }

  isAtEnd() => current >= source.length;

  void error(int line, String message) {
    report(line, "", message);
  }

  void report(int line, String where, String message) {
    print("[line ${line}] Error ${where}: ${message}");
    hadError = true;
  }

  bool isDigit(String s) {
    int val = s.codeUnitAt(0);
    return val >= '0'.codeUnitAt(0) && val <= '9'.codeUnitAt(0);
  }

  bool isAlpha(String s) {
    int c = s.codeUnitAt(0);
    return (c >= 'a'.codeUnitAt(0) && c <= 'z'.codeUnitAt(0)) ||
        (c >= 'A'.codeUnitAt(0) && c <= 'Z'.codeUnitAt(0)) ||
        c == '_'.codeUnitAt(0);
  }

  bool isAlphaNumericOrDot(String c) {
    return isAlpha(c) || isDigit(c) || c == ".";
  }

  number() {
    while (isDigit(peek())) advance();

    // Look for a fractional part.
    if (peek() == '.' && isDigit(peekNext())) {
      // Consume the "."
      advance();

      while (isDigit(peek())) advance();
    }

    addTokenWithLiteral(
        TokenType.NUMBER, double.parse(source.substring(start, current)));
  }

  identifier() {
    while (isAlphaNumericOrDot(peek())) advance();
    String text = source.substring(start, current);
    TokenType type = TokenType.IDENTIFIER;

    if (keywords.containsKey(text))
        type = keywords[text];

    addToken(type);
  }

  void string() {
    while (peek() != '"' && !isAtEnd()) {
      if (peek() == '\n') line++;
      advance();
    }

    // Unterminated string.
    if (isAtEnd()) {
      error(line, "Unterminated string.");
      return;
    }

    // The closing ".
    advance();

    String value = source.substring(start + 1, current - 1);
    addTokenWithLiteral(TokenType.STRING, value);
  }

  static Map<String,TokenType> keywords = {
    "using": TokenType.USING,
    "public":  TokenType.PUBLIC,
    "private":  TokenType.PRIVATE,
    "override":  TokenType.OVERRIDE,
    "new": TokenType.NEW,
    "static": TokenType.STATIC,
    "class": TokenType.CLASS,
    "if":  TokenType.IF,
    "for":  TokenType.FOR,
    "else":  TokenType.ELSE,
    "return":  TokenType.RETURN,
    "namespace":  TokenType.NAMESPACE,
    "get":  TokenType.GET,
    "set":  TokenType.SET,
    "ref":  TokenType.REF,
    "out":  TokenType.OUT
  };
}
