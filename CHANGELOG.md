# Changelog

## 0.0.2
- ignore attributes

## 0.0.1

- Initial version, created by Stagehand

## 0.0.4

- Attributes on classes
