// Copyright (c) 2017, Tomasz Kubacki. All rights reserved. Use of this source code
// is governed by a MIT-style license that can be found in the LICENSE file.

import 'package:csharp_parser/csharp_parser.dart';
import 'package:csharp_parser/src/ast_printer.dart';

main() {
  String sourceCode = """
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XimContract.Models;
using XimContract.Models.Data;

namespace Heaven.Messages.Data
{
    [XmlRoot("MyClassAttribute")]
    public class ValuesRequest : BaseMessageRequest
    {
    
        public string TypeORString
        {
           get {
            bool t = true && false || false;
            String t = "A"+"B"; 
            int r = 89 * 23;
            return (Type.ToString()); 
           }
           
           set
           {
                EMyEnum typeVal;
                if (Enum.TryParse(value, out typeVal) || a == b)
                    Type = typeVal;
           }
        }
        
        private int? counter = 90;
        
        private String? hashCode = "234fsdfsfs";
    
        public String? HashCode
        {
           get { return hashCode; }
           set
           {
                hashCode = value;
           }
        }
    
        [MyOtherAttribute("Bum Bum")]
        [XmlAttribute("Name")]
        public string NewName { get; set; }
        
        [XmlAttribute("type")]
        [Newtonsoft.Json.JsonIgnore]
        public string TypeString
        {
           get { return Type.ToString(); }
           set
            {                
                EMyEnum typeVal;
                if (Enum.TryParse(value, out typeVal))
                    Type = typeVal;
            }
        }

    
        static int VERSION = 0;
        
        String name;
        
        public List<VarValue> valuesFound = new List<VarValue>();

        public List<VarValue> LocalizedValues { get; set; } = new List<VarValue>();

        public List<int> DeletedValues { get; set; } = new List<int>();

        public override string GetSomething()
        {
            return (Constants.Heaven.SomeThing);
        }
    }
}

  """;

  var scanner = new Scanner(sourceCode);
  scanner.scanTokens();
  print('Scanner error: ${scanner.hadError}\n');

  /*if(!scanner.hadError){
    for(var token in scanner.tokens){
      print(token);
    }
  } */

  Parser parser = new Parser(scanner.tokens);

  List<Stmt> statements = parser.parse();

  AstPrinter printer = new AstPrinter();

  statements.forEach((stm) {
    if (stm.typeName == "namespace") {
      print("namespace: " + printer.visitNamespace(stm)+"\n");
      (stm as Namespace).classes.forEach((Class c) {
        print(printer.visitClassStmt(c));
        print("---\nfields:");
        c.fields.forEach((Variable v) {
          print(printer.visitType(v.type) + " " + printer.visitVariable(v));
        });

        print("-\nproperties:");
        c.properties.forEach((Property p) {
          print(printer.visitType(p.type) + " " + printer.visitProperty(p));
        });

        print("-\nmethods:");
        c.methods.forEach((Method m) {
          print( printer.visitMethod(m));
        });
      });
    }
  });
}
