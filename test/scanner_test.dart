// Copyright (c) 2017, Tomasz Kubacki. All rights reserved. Use of this source code
// is governed by a MIT-style license that can be found in the LICENSE file.

import 'package:csharp_parser/csharp_parser.dart';
import 'package:test/test.dart';

void main() {
  group('A group of tests', () {
    Scanner scanner;

    setUp(() {
      scanner = new Scanner("");
    });

    test('Scanner is not at the end before started scanning', () {
      scanner.source = "class A { int B {get;set;} }";
      expect(scanner.isAtEnd(), isFalse);
    });

    test('Scanner is  at the end before after scanning', () {
      scanner.source = "class A { int B {get;set;} }";
      scanner.scanTokens();
      expect(scanner.isAtEnd(), isTrue);
    });

    test('Scanner ignores comments and empty lines', () {
      scanner.source = """
      //comment
    
      /// <summary>
      /// this is comment too
      /// </summary>
      class
      
      """;
      scanner.scanTokens();
      expect(scanner.tokens.length, equals(2));
    });

    test('Scanner can tokenize simple class', () {
      scanner.source = """
class TestPerson
{
    static void Main()
    {
        // Call the constructor that has no parameters.
        Person person1 = new Person();
        Console.WriteLine(person1.name);

        person1.SetName("John Smith");
        Console.WriteLine(person1.name);

        // Call the constructor that has one parameter.
        Person person2 = new Person("Sarah Jones");
        Console.WriteLine(person2.name);

        // Keep the console window open in debug mode.
        Console.WriteLine("Press any key to exit.");
        Console.ReadKey();
    }
}   
      """;
      scanner.scanTokens();
      expect(scanner.hadError, isFalse);
    });
  });
}
